import { Server } from 'http'
import * as express from 'express'
import * as bodyParser from 'body-parser'

import IWebServerConstructor from '../intf/IWebServerConstructor'
import IEnvironment from '../intf/IEnvironment'
import IStaticRoute from '../intf/IStaticRoute'
import IStaticRoutes from '../intf/IStaticRoutes'

const app = express()
const helmet = require('helmet')
const expressStaticGzip = require('express-static-gzip')

const gzipOptions = {
  enableBrotli: true,
  customCompressions: [{
      encodingName: 'deflate',
      fileExtension: 'zz'
  }],
  orderPreference: ['br']
}

export default class WebServer {

  private config: IEnvironment

  private staticRoutes: IStaticRoutes

  constructor({ config, staticRoutes }: IWebServerConstructor) {
    this.config = config
    this.staticRoutes = staticRoutes
  }

  public start(): Server {
    const { appDir, compression, port } = this.config
    const routes = {...this.staticRoutes}
    const cmdDir = process.cwd()
    const applicationDir = `/dist/${appDir}/app`
    routes.app = {
      uri: '/app',
      directory: applicationDir
    }

    app.use(helmet())

    app.use(bodyParser.urlencoded({ extended: true }))

    app.use(bodyParser.json())

    app.use((req: any, res: any, next: any) => {
      // <<<<<<<< ADDING DYNAMIC HEADERS

      // res.header('Access-Control-Allow-Origin', '*')
      // res.header('Access-Control-Allow-Credentials', 'true')
      // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
      // res.header('Access-Control-Allow-Headers', 'Origin,Content-Type,Accept,Authorization,content-type')
      next()
    })

    console.info('\n\n launching a server instance...')
    console.info(` React application directory: ${cmdDir}${applicationDir}\n`)

    // Attach all static paths from config
    Object.keys(routes).forEach((name: string) => {
      const route: IStaticRoute = routes[name]
      console.info(`   + static route mapping: ${route.uri} to ${route.directory} ${compression ? '(with compression)' : ''}`)
      app.use(route.uri,
        !compression
          ? express.static(cmdDir + route.directory)
          : expressStaticGzip(cmdDir + route.directory, gzipOptions)
      )
    })

    // For all requests direct to web application
    app.get('*', (req: any, res: any) => {
      res.sendFile(`${process.cwd()}/dist/${appDir}/index.html`)
    })

    return app.listen(port, () => {
      console.info(`\n server is listening at port: ${port}\n\n`)
      // callback()
    })
  }
}
