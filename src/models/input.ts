import * as _ from 'lodash'
import IInputModel, { IInputModelConstructor } from '../intf/IInputModel'
import IValidationSchema from '../intf/IValidationSchema'
import IValidationRule from '../intf/IValidationRule'
import IInputModelData from '../intf/IInputModelData'
import IInputModelDataItem from '../intf/IInputModelDataItem'
import IValidationCheck from '../intf/IValidationCheck'
import IKeyAny from '../intf/IKeyAny'
import IValidatorCheck from '../intf/IValidatorCheck'

import LifeCycles from '../props/LifeCycles'
import Validation from '../modules/validation/validation'

export default class InputModel implements IInputModel {

  private data: IInputModelData = {}

  private name: string

  private validationSchema: IValidationSchema

  private lifecycle: LifeCycles = LifeCycles.idle

  private response: any

  private validation: IValidationCheck | null = null

  public constructor({name, data, schema}: IInputModelConstructor) {
    this.name = name
    this.data = data
    this.validationSchema = (schema)
      ? schema
      : {}
  }

  // Get model data
  public getInputData(): IInputModelData | {} {
    return (this.data)
      ? this.data
      : {}
  }

  // Get an input set for a field
  public getInputDataItem(key: string): IInputModelDataItem {
    return (this.data && this.data.hasOwnProperty(key))
      ? this.data[key]
      : {
        _: ''
      }
  }

  // Get a value from a field
  public getValue(key: string): any {
    const data = this.getInputDataItem(key)
    if (!data) {
      throw new Error(`Input data field '${key}' has not been defined in a model '${this.name}'`)
    }
    return data._
  }

  // Return validation record after performing validate()
  public getValidation(): IValidationCheck {
    return (this.validation)
      ? this.validation
      :
        {
          valid: false,
          checks: [],
          failed: []
        }
  }

  // Set a value
  public setValue(key: string, value: any, ignoreValidation?: boolean): void {
    const data = this.getInputDataItem(key)
    const rule = this.getValidationRule(key)
    if (!data) {
      throw new Error(`Input data field '${key}' has not been defined in a model '${this.name}'`)
    }
    const allowUpdate = (data.exact && rule && rule.input && !ignoreValidation)
      ? this.validateInput(rule.input, value)
      : !data.exact

    if (allowUpdate) {
      this.data[key]._ = value
    }
  }

  // Set a custom schema if needed
  public setSchema(schema: IValidationSchema): void {
    this.validationSchema = schema
  }

  // Set a lifecycle for request (if made)
  public setLifeCycle(lifecycle: LifeCycles): void {
    this.lifecycle = lifecycle
  }

  // Set model to be processed
  public setLifeCycleProcessed(): void {
    this.lifecycle = LifeCycles.processed
    this.validation = null
  }

  // Set a response from the API once the response arrived
  public setResponse(data: any): void {
    this.response = data
  }

  public forRequest(): IKeyAny {
    const keys = Object.keys(this.data)
    let data: IKeyAny = {}
    if (keys && keys.length > 0) {
      keys.forEach((key: string) => {
        data[key] = this.data[key]._
      })
    }
    return data
  }

  // Get a response from the API after the request is completed
  public getResponse(): any {
    return this.response
  }

  // Check request lifecycle (triggered)
  public isRequestTriggered(): boolean {
    return this.lifecycle === LifeCycles.triggered
  }

  // Check if request is completed
  public isRequestCompleted(): boolean {
    return this.lifecycle === LifeCycles.completed
  }

  // Check if can submit the form
  public canSubmit(): boolean {
    return this.isValidationPerformed() && this.isValid()
  }

  // Check if any operation was performed on the model other than value change
  // Model is idle when initialised, before validation or request is triggered
  public isIdle(): boolean {
    return this.lifecycle === LifeCycles.idle
  }

  // Check if model request was processed. This should mean that user already
  // seen the result of validation and form submission
  public isProcessed(): boolean {
    return this.lifecycle === LifeCycles.processed
  }

  // Check if model went under validation step at all
  // Note: This is does not mean that the model is valid
  public isValidationPerformed(): boolean {
    return (this.lifecycle === LifeCycles.validated) ? true : false
  }

  // Check if model data are valid
  public isValid(): boolean {
    return this.validation ? this.validation.valid : true
  }

  public isFieldValid(key: string): boolean {
    if (this.validation && this.validation.failed) {
      const field = this.validation.failed.find((f: IValidatorCheck) => f.key === key)
      return (!field) ? true : false
    }
    return true
  }

  // Validate model data
  public validate(fields?: string[]): void {
    if (this.validationSchema && Object.keys(this.validationSchema).length === 0) {
      console.error(`Validation schema for ${this.name} is not set`)
    }
    this.setLifeCycle(LifeCycles.validated)
    const validation = new Validation(this.data, this.validationSchema, fields)
    this.validation = validation.check()
  }

  // Validate input characters before saving if `exact` is true
  private validateInput(regex: RegExp, value: any): boolean {
    return value.toString().match(regex)
  }

  // Get a validation rule for a field
  private getValidationRule(key: string): IValidationRule | undefined {
    const record = this.getInputDataItem(key)
    return (record.v && this.validationSchema && this.validationSchema.hasOwnProperty(record.v))
      ? this.validationSchema[record.v]
      : undefined
  }

}
