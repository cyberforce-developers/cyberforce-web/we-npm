import Languages from '../../props/Languages'

const generate = require('nanoid/generate')
import * as crypto from 'crypto-js'
import IKeyString from '../../intf/IKeyString'

export namespace Utils {
  export function ID({ pattern }: { pattern: string }): any {
    const t = pattern.split('.')
    return generate(t[1], Number(t[0]))
  }

  export function parsePrismicLanguage(lang: string): Languages {
    if (lang === 'en-gb') {
      return Languages.gb
    } else {
      return Languages.us
    }
  }

  export function hash(str: string): string {
    return crypto.MD5(str).toString()
  }

  // text = Email %email% has been replaced %times% times
  // to: Email dave@dev.com has been replaced
  /* pattern = {
      email: 'dave@dev.com'
      times: 'two'
     }
  */
  export function replacePattern(text: string, pattern: IKeyString): string {
    return ''
  }
}
