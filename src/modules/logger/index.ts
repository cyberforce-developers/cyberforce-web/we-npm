import chalk from 'chalk'
import IAppLog from '../../intf/IAppLog'
import IKeyAny from '../../intf/IKeyAny'
import LogLevels from '../../props/LogLevels'

export namespace Logger {

  const colors: IKeyAny = {}
  colors[LogLevels.error] = (str: string): string => chalk.red(str)
  colors[LogLevels.info] = (str: string): string => chalk.gray(str)
  colors[LogLevels.success] = (str: string): string => chalk.green(str)
  colors[LogLevels.report] = (str: string): string => chalk.grey(str)
  colors[LogLevels.warning] = (str: string): string => chalk.yellow(str)

  export function screen(log: IAppLog): void {
    const date = new Date().toUTCString()
    const llf = colors[log.level](log.level.toUpperCase())
    const message = log.message
      ? chalk.cyan(log.message)
      : ''
    const display = `${chalk.gray(date)}\n${llf} ${chalk.yellow(log.slug)}\n${message}\n`
    console.info(display)
  }

  export function startup(message: string, skip?: boolean): void {
    const date = new Date().toUTCString()
    const abbrev = skip
      ? chalk.greenBright('START')
      : chalk.cyan('START')
    console.info(
      skip
        ? `${abbrev} ${chalk.gray(message)}\n`
        : `${chalk.gray(date)}\n${abbrev} ${chalk.white(message)}\n`
    )
  }

}
