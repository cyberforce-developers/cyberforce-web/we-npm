import * as _ from 'lodash'
import axios, { AxiosPromise, AxiosInstance } from 'axios'
import MockAdapter from 'axios-mock-adapter'
import IKeyAny from '../../intf/IKeyAny'
import IWebRequest, { IWebRequestArgs, IWebRequestSend, IWebRequestSendToApiRoute, IWebRequestMake } from '../../intf/modules/IWebRequest'
import RequestMethods from '../../props/RequestMethods'

export default class WebRequest implements IWebRequest {

  private testing: boolean

  private axios: AxiosInstance

  private headers: IKeyAny = {
    'Content-Type': 'application/json; charset=utf-8'
  }

  constructor({ baseUrl, timeout }: IWebRequestArgs = {
    baseUrl: 'http://localhost',
    timeout: 30000
  }) {
    this.axios = axios.create({
      baseURL: baseUrl,
      timeout
    })
  }

  public setTesting = (sw: boolean): void => {
    this.testing = sw
  }

  public sendToApiRoute = ({route, payload, options}: IWebRequestSendToApiRoute): AxiosPromise<any> => {
    return this.make({
      url: route.uri,
      method: route.method,
      payload,
      options
    })
  }

  public send = ({url, method, payload, options}: IWebRequestSend): AxiosPromise<any> => {
    return this.make({ url, method, payload, options })
  }

  private make = ({url, method, payload, options}: IWebRequestMake): AxiosPromise<any> => {
    let mock: MockAdapter | undefined
    if (this.testing) {
      mock = new MockAdapter(this.axios, { delayResponse: 30 })
    }

    if (options && options.headers) {
      this.addHeaders(options.headers)
    }

    const opt: IKeyAny = {
      headers: this.headers
    }

    switch (method) {
      case RequestMethods.POST: {
        if (this.testing && mock) {
          mock.onPost(url).reply(200, {
            endpointStatus: true,
            payload,
            options: opt
          })
        }
        return this.axios.post(url, payload, opt)
      }
      case RequestMethods.GET:
      default:
        if (!_.isEmpty(payload)) {
          opt.params = payload
        }
        if (this.testing && mock) {
          mock.onGet(url).reply(200, {
            endpointStatus: true,
            options: opt
          })
        }
        return this.axios.get(url, opt)
    }
  }

  private addHeaders = (headers: object): void => {
    this.headers = {
      ...this.headers,
      ...headers
    }
  }
}
