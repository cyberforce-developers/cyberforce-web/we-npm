import Locale from '../locale'
import IModuleValidationSummary from '../../intf/modules/IModuleValidationSummary'
import ValidationErrors from '../../props/ValidationErrors'
import IValidatorCheck from '../../intf/IValidatorCheck'
import IKeyAny from '../../intf/IKeyAny'
import IValidationCheck from '../../intf/IValidationCheck'

export default class ValidationSummary implements IModuleValidationSummary {

  private valid: boolean

  private check: IValidationCheck

  private locale: Locale

  constructor(validation: IValidationCheck, locale: Locale) {
    this.check = validation
    this.locale = locale
    this.valid = (validation) ? validation.valid : true
  }

  public isValid = (): boolean => {
    return this.valid
  }

  public getList = (): string[] => {
    const locale = this.locale
    if (!this.valid) {
      const list = this.check.failed.map((item: IValidatorCheck) => {
        const { key, error } = item
        const label = item.label
        let inputTranslation: IKeyAny = locale.text('inputs', label)
        if (!inputTranslation || (inputTranslation && !inputTranslation.hasOwnProperty('label'))) {
          inputTranslation = { label: key }
        }
        let message: string
        const t = locale.text('validation')
        switch (error) {
          case ValidationErrors.tooLong:
            message = `${t.tooLong}: ${t.given} ${item.length.given}, ${t.expected}: ${item.length.expected}`
            break

          case ValidationErrors.mustMatch:
            message = (item && item.mustMatchFields)
              ? t.mustMatch
              : `${t.mustMatch}: [${item.mustMatchFields ? item.mustMatchFields.join(', ') : ''}]`
            break

          case ValidationErrors.tooShort:
            message = `${t.tooShort}: ${t.given} ${item.length.given}, ${t.expected} ${item.length.expected}`
            break

          case ValidationErrors.eitherOr:
            const i = locale.text('inputs')
            const tKey = item.eitherOr
            const eitherOr = (tKey && i.hasOwnProperty(tKey)) ? i[tKey].label : tKey
            message = `${t.eitherOr} (${eitherOr.toLowerCase()})`
            break

          case ValidationErrors.mustNotMatch:
            if (item.mustNotMatchFields) {
              const fields = item.mustNotMatchFields.map((input: any) => {
                return locale.text('inputs', input)
              })
              message = `${t.mustNotMatch} ${fields.join(', ')}`
            } else {
              message = t.mustNotMatch
            }
            break

          case ValidationErrors.regex:
            message = (inputTranslation.hasOwnProperty('validation'))
              ? inputTranslation.validation
              : t.regex
            break

          case ValidationErrors.input:
            message = t.input
            break

          case ValidationErrors.emptyRule:
            message = t.emptyRule
            break

          default:
            message = t.unknown
        }
        return `${inputTranslation.label}: ${message}`
      })
      return list
    } else {
      return []
    }
  }
}
