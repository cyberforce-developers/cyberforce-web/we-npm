import IModelDataItems from '../../intf/IInputModelData'
import IValidationCheck from '../../intf/IValidationCheck'
import IValidationSchema from '../../intf/IValidationSchema'
import Validator from '../../modules/validation/validator'
import IValidatorCheck from '../../intf/IValidatorCheck'

export default class Validation {

  private data: IModelDataItems

  private fields: string[] | undefined

  private schema: IValidationSchema

  constructor(data: IModelDataItems, schema: IValidationSchema, fields?: string[]) {
    this.data = data
    this.fields = fields
    this.schema = schema
  }

  public check(): IValidationCheck {
    const checks: IValidatorCheck[] = []
    const fields = (this.fields && this.fields.length > 0 && this.data)
      ? this.fields
      : Object.keys(this.data)

    fields.forEach((key: string) => {
      const item = this.data[key]
      const rule = (this.schema && item.v && this.schema.hasOwnProperty(item.v))
        ? this.schema[item.v]
        : undefined

      const validator = new Validator(key, this.data, rule)
      checks.push(validator.check())
    })

    const failed = checks.filter((check: IValidatorCheck) => check.valid === false)

    return {
      valid: failed.length === 0,
      checks,
      failed
    }
  }
}
