import * as _ from 'lodash'
import IValidatorCheck from '../../intf/IValidatorCheck'
import IValidationRule from '../../intf/IValidationRule'
import IKeyAny from '../../intf/IKeyAny'
import IInputModelData from '../../intf/IInputModelData'
import IInputModelDataItem from '../../intf/IInputModelDataItem'

import ValidationErrors from '../../props/ValidationErrors'

interface IValidator {
  check(): any // IValidationCheck
  setIsValidWhenNoRule(sw: boolean): void
}

export default class Validator implements IValidator {

  private data: IInputModelData | {}

  private record: IInputModelDataItem

  private key: string

  private rule: IValidationRule | undefined

  private validationErrors: IKeyAny

  private isValidWhenNoRule: boolean = true

  constructor(key: string, data: IInputModelData, rule: IValidationRule | undefined) {
    this.key = key
    this.data = (data) ? data : {}
    this.rule = (rule) ? rule : undefined
    this.record = (this.data.hasOwnProperty(this.key))
      ? this.data[key]
      : { _: '', v: '', exact: false }

    this.validationErrors = ValidationErrors
  }

  public setIsValidWhenNoRule = (sw: boolean): void => {
    this.isValidWhenNoRule = sw
  }

  public check = (): IValidatorCheck => {
    const given: number = (this.record._) ? String(this.record._).length : 0
    const length = {
      given
    }

    const label = this.record.l ? this.record.l : this.key

    if (!this.rule) {
      return (this.isValidWhenNoRule)
        ? {
          valid: true,
          key: this.key,
          validationKey: this.record.v,
          label,
          length
        }
        : {
          valid: false,
          key: this.key,
          validationKey: undefined,
          label,
          error: (this.validationErrors.hasOwnProperty('emptyRule'))
            ? this.validationErrors['emptyRule']
            : 'EMPTY_RULE',
          length
        }
    }

    let validation: IValidatorCheck = {
      valid: true,
      key: this.key,
      validationKey: this.record.v,
      label,
      length
    }

    const value = this.record._

    if (this.rule.mustMatch) {
      validation = this.checkRuleMustMatch(validation, this.rule.mustMatch, value)
    }

    if (this.rule.mustNotMatch) {
      validation = this.checkRuleMustNotMatch(validation, this.rule.mustNotMatch, value)
    }

    if (this.rule.eitherOr) {
      validation = this.checkRuleEitherOr(validation, this.rule.eitherOr, value)
    }

    if (this.rule.min) {
      validation = this.checkRuleMin(validation, this.rule.min)
    }

    if (this.rule.max) {
      validation = this.checkRuleMax(validation, this.rule.max)
    }

    validation = this.checkRegex(validation, this.rule.check, value)

    return validation
  }

  private checkRuleMustMatch = (validation: IValidatorCheck, mustMatch: string[], value: any): IValidatorCheck => {
    if (this.isRequired() || this.isOptionalButNotEmpty(value)) {
      let contains: boolean = false
      mustMatch.forEach((valueOrKey: string) => {
        if (!contains && value !== null && value !== undefined) {
          const anotherValue = this.getRelatedValue(valueOrKey)
          contains = this.valuesMatch(value, anotherValue)
        }
      })
      if (contains === false) {
        return {
          ...validation,
          valid: false,
          error: this.validationErrors.mustMatch,
          mustMatchFields: mustMatch
        }
      }
    }
    return validation
  }

  private checkRuleMustNotMatch = (validation: IValidatorCheck, mustNotMatch: string[], value: any): IValidatorCheck => {
    const mustNotMatchFields: string[] = []
    mustNotMatch.map((valueOrKey: string) => {
      const notEqualValue = this.getRelatedValue(valueOrKey)
      if (this.valuesMatch(value, notEqualValue)) {
        mustNotMatchFields.push(valueOrKey)
      }
    })
    if (mustNotMatchFields.length > 0) {
      return {
        ...validation,
        valid: false,
        error: this.validationErrors.mustNotMatch,
        mustNotMatchFields
      }
    }
    return validation
  }

  private checkRuleEitherOr = (validation: IValidatorCheck, eitherOrValueOrKey: any, value: any): IValidatorCheck => {
    const anotherValue = this.getRelatedValue(eitherOrValueOrKey)
    if (this.isEmpty(value) && this.isEmpty(anotherValue)) {
      return {
        ...validation,
        valid: false,
        error: this.validationErrors.eitherOr,
        eitherOr: eitherOrValueOrKey
      }
    }
    return validation
  }

  private checkRuleMin = (validation: IValidatorCheck, min: number): IValidatorCheck => {
    if (validation.length.given < min) {
      return {
        ...validation,
        valid: false,
        error: this.validationErrors.tooShort,
        length: {
          given: validation.length.given,
          expected: min
        }
      }
    }
    return validation
  }

  private checkRuleMax = (validation: IValidatorCheck, max: number): IValidatorCheck => {
    if (validation.length.given > max) {
      return {
        ...validation,
        valid: false,
        error: this.validationErrors.tooLong,
        length: {
          given: validation.length.given,
          expected: max
        }
      }
    }
    return validation
  }

  private checkRegex = (validation: IValidatorCheck, regex: RegExp | undefined, value: any): IValidatorCheck => {
    if (regex && !regex.test(value)) {
      return {
        ...validation,
        valid: false,
        error: this.validationErrors.regex
      }
    }
    return validation
  }

  private valuesMatch = (one: any, another: any): boolean => {
    if (!one && !another) {
      return true
    } else if (one.toString() === another.toString()) {
      return true
    } else if (one == another) {
      return true
    } else if (_.isArray(another) && another.indexOf(one) !== -1) {
      return true
    } else {
      return false
    }
  }

  /*
    In validation schema you can define fields or values that may depend on the one which is being verified (or another way around).
    For example, you may want validation to fail if the field you check contains a particular value or is different/equal to another field.
    The function determines whether the given value is just a plain value or is a dynamic reference to an existing field within the data structure
  */
  private getRelatedValue = (valueOrKey: any): any => {
    if (_.isString(valueOrKey) && this.isReferenceToAnotherRecord(valueOrKey)) {
      // It is a field.
      const key = this.obtainFieldKey(valueOrKey)
      const validationCommonItem = this.getAnotherRecordValue(key)
      return validationCommonItem
    } else {
      // It is a value.
      return valueOrKey
    }
  }

  private isReferenceToAnotherRecord = (valueOrKey: string): boolean => {
    return (valueOrKey.indexOf('*') !== -1) ? true : false
  }

  private obtainFieldKey = (valueOrKey: string): string[] => {
    const keys = valueOrKey.replace('*', '')
    return keys.split('.')
  }

  private getAnotherRecordValue = (keys: string[]): any => {
    const record = (keys.length > 1)
      ? this.getRecordValueFromNestedStructure(keys)
      : this.getRecordValueFromSimpleStructure(keys)
    if (record && record.hasOwnProperty('_')) {
      return record._
    } else {
      console.error('Given key is not a IValidationCommonItem')
      return undefined
    }
  }

  private getRecordValueFromSimpleStructure = (keys: string[]): any => {
    return (keys && keys.length === 1 && this.data && this.data.hasOwnProperty(keys[0]))
      ? this.data[keys[0]]
      : undefined
  }

  private getRecordValueFromNestedStructure = (keys: string[]): any => {
    if (keys.length === 1) {
      return this.getRecordValueFromSimpleStructure([keys[0]])
    } else if (keys.length === 2) {
      const lev1 = keys[0]
      const lev2 = keys[1]
      if (this.data.hasOwnProperty(lev1) && this.data[lev1].hasOwnProperty(lev2)) {
        return this.data[lev1][lev2]
      }
    } else if (keys.length === 3) {
      // TODO: Refactor for more efficient method or use something existing
      const lev1 = keys[0]
      const lev2 = keys[1]
      const lev3 = keys[2]
      if (this.data.hasOwnProperty(lev1) && this.data[lev1].hasOwnProperty(lev2) && this.data[lev1][lev2].hasOwnProperty(lev3)) {
        return this.data[lev1][lev2][lev3]
      }
    } else {
      return undefined
    }
  }

  private isNotEmpty(value: any): boolean {
    const sValue = String(value)
    return sValue.length > 0 && sValue != 'null' && sValue != 'undefined'
  }

  private isEmpty(value: any): boolean {
    const sValue = String(value)
    return sValue.length === 0 || sValue == 'null' || sValue == 'undefined'
  }

  private isOptionalButNotEmpty = (value: any): boolean => {
    const hasRule = (this.rule) ? true : false
    const hasNoMin = this.rule && !this.rule.min
    const minIsZero = this.rule && this.rule.min && this.rule.min === 0
    return ((hasRule && (hasNoMin || minIsZero)) && this.isNotEmpty(value)) ? true : false
  }

  private isRequired = (): boolean => {
    return (this.rule && this.rule.min && this.rule.min > 0) ? true : false
  }
}
