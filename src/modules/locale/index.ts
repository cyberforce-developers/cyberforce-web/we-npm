import * as _ from 'lodash'
import IModuleLocale from '../../intf/modules/IModuleLocale'
import IKeyAny from '../../intf/IKeyAny'

export default class Locale implements IModuleLocale {

  private unknownChar: string = '?'

  private locale: IKeyAny = {}

  private currentLanguage: string | undefined = undefined

  public setCurrentLanguage = (language: string) => {
    if (this.hasTranslation(language)) {
      this.currentLanguage = language
    } else {
      console.info('Add a translation first before setting a current language')
    }
  }

  public addTranslation = (language: string, translation: IKeyAny) => {
    this.locale[language] = (this.locale.hasOwnProperty(language))
      ? Object.assign({}, this.locale[language], translation)
      : translation
  }

  public hasAnyTranslation = (): boolean => {
    return (Object.keys(this.locale).length > 0) ? true : false
  }

  public hasTranslation = (language: string): boolean => {
    return this.locale.hasOwnProperty(language) ? true : false
  }

  public text = (key: string, subkey?: string): string | object | any => {
    if (this.currentLanguage) {
      const translation = this.locale[this.currentLanguage]
      const hasKey: boolean = translation.hasOwnProperty(key)
      if (hasKey && !subkey) {
        return translation[key]
      }

      if (subkey && hasKey && translation[key].hasOwnProperty(subkey)) {
          return translation[key][subkey]
      } else if (subkey && hasKey && !translation[key].hasOwnProperty(subkey)) {
          return this.unknownChar
      }
    }
    return this.unknownChar
  }
}
