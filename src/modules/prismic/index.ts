import IModulePrismic from '../../intf/modules/IModulePrismic'
import IPrismicRawData from '../../intf/IPrismicRawData'
import IPrismicParsedData from '../../intf/IPrismicParsedData'
import { Utils } from '../utils'

// const PrismicJS = require('prismic-javascript');

class Prismic implements IModulePrismic {

  private static parse(raw: IPrismicRawData): IPrismicParsedData[] {
    return (raw && raw.hasOwnProperty('results'))
      ? raw.results.map((data) => {
          return {
            id: data.id,
            uid: data.uid,
            type: data.type,
            lang: Utils.parsePrismicLanguage(data.lang),
            data: data.data
          }
        })
      : []
  }

  private raw: IPrismicRawData

  private parsed: IPrismicParsedData[]

  public constructor(data: IPrismicRawData) {
    this.raw = data
    this.parsed = Prismic.parse(this.raw)
  }

  public getData(): IPrismicParsedData[] {
    return this.parsed
  }

  public getContent(uid: string): IPrismicParsedData[] | [] {
    return (this.parsed.filter((element: IPrismicParsedData) => element.uid === uid))
  }
}

export default Prismic

// https://itn-web-marketing.cdn.prismic.io/api/v2
// MC5YYWI3MWhJQUFDTUFySUhm.AHUEBFfvv70USSDvv73vv70s77-977-977-977-9SU0N77-9XO-_ve-_ve-_ve-_ve-_vS4z77-977-977-977-9
