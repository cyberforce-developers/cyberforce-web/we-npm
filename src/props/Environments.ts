enum Environments {
  local = 'local',
  pr = 'pr',
  dev = 'dev',
  uat = 'uat',
  production = 'production',
  testing = 'testing'
}

export default Environments
