import IValidationPattern from '../intf/IValidationPattern'

export const onlyNumbers: IValidationPattern = {
  input: /^[0-9]*$/,
  check: /^[0-9]+$/
}

export const smallLetters: IValidationPattern = {
  input: /^[a-z]*$/,
  check: /^[a-z]+$/
}

export const onlyLetters: IValidationPattern = {
  input: /^[a-zA-Z]*$/,
  check: /^[a-zA-Z]+$/
}

export const email: IValidationPattern = {
  input: /^[0-9a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ\.\@\_\-]*$/,
  check: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}

export const password: IValidationPattern = {
  input: /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,50})/,
  check: /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,50})/
}
