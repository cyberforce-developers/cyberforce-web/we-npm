enum RequestMethods {
  'GET' = 'get',
  'POST' = 'post'
}

export default RequestMethods
