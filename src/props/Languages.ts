enum Languages {
  gb = 'en-GB',
  us = 'en-US'
}

export default Languages
