enum LifeCycles {
  idle = 'idle',
  validated = 'validated',
  triggered = 'triggered',
  completed = 'completed',
  processed = 'processed'
}

export default LifeCycles
