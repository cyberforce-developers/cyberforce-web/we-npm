enum ValidationErrors {
  tooLong = 'TOO_LONG',
  tooShort = 'TOO_SHORT',
  mustMatch = 'MUST_MATCH',
  mustNotMatch = 'MUST_NOT_MATCH',
  eitherOr = 'EITHER_OR',
  input = 'INPUT',
  regex = 'REGEX',
  emptyRule = 'EMPTY_RULE'
}

export default ValidationErrors
