// This is a web application environment definition. Use only for web applications (client)
// Properties here are exposed publicly

// This enum is shared because WEBAPP_ENV is reusable for webapp and webapp server.
import Environments from '../props/Environments'

export default interface IWebAppEnvironment {
  name: Environments
  description: string
  api: {
    url: string // Default main URl for the API service
    timeout: number // Default request timeout in miliseconds
  }
}
