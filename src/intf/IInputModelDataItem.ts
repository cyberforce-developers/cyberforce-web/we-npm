// Input and Data models have to represent the same data structure in order for validation module
// to understand how to validate them.
// Input models represent this structure by default. Data model needs to be converted to it.

export default interface IInputModelDataItem {
  _: any // value
  v?: string  // ValidationSchema key
  l?: string // field label in the translation (inputs)
  exact?: boolean // Determines if an input changes only when valid characters are pressed
}
