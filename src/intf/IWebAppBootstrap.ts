import IRoute from './IRoute'
import IGlobal from './IWebAppGlobal'
import IWebAppConfig from './IWebAppConfig'
import IWebAppEnvironment from './IWebAppEnvironment'
import IKeyAny from './IKeyAny'
import Locale from '../modules/locale'

export default interface IWebAppBootstrap {
  routes: IRoute[] // list of web app routes for react router, check ./routes.ts file in your web application
  global: IGlobal // global configuration dependency from ./global.ts
  config: IWebAppConfig // application config from ./config.ts
  current: IWebAppEnvironment // current environment
  locale: Locale // translation class
  enhancers?: IKeyAny // optional enhancers (custom reducers needed for more advanced system functions i.e. forms etc.)
  bootstrapComponent: any
}
