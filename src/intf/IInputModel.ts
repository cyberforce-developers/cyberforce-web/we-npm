import IKeyAny from './IKeyAny'
import IValidationSchema from './IValidationSchema'
import LifeCycles from '../props/LifeCycles'
import IInputModelData from './IInputModelData'
import IInputModelDataItem from './IInputModelDataItem'
import IValidationCheck from './IValidationCheck'

export default interface IInputModel {
  forRequest(): IKeyAny
  setSchema(schema: IValidationSchema): void
  setValue(key: string, value: any, ignoreValidation?: boolean): void
  setLifeCycle(lifecycle: LifeCycles): void
  setLifeCycleProcessed(): void
  setResponse(data: any): void
  getResponse(): any
  getValidation(): IValidationCheck
  getValue(key: string): any
  getInputData(): IInputModelData | {}
  getInputDataItem(key: string): IInputModelDataItem | undefined
  isRequestTriggered(): boolean
  isRequestCompleted(): boolean
  isValidationPerformed(): boolean
  isProcessed(): boolean
  isIdle(): boolean
  isValid(): boolean
  isFieldValid(key: string): boolean
  validate(fields?: string[]): void
}

export interface IInputModelConstructor {
  name: string
  data: IInputModelData
  schema?: IValidationSchema
}
