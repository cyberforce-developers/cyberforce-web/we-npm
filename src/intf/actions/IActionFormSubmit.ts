import { IWebRequestSend } from '../modules/IWebRequest'

export default interface IActionFormSubmit {
  form: string
  request: IWebRequestSend
  validate?: boolean
  fields?: string[]
}
