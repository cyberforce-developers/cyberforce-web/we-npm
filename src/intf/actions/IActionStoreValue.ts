
export default interface IActionStoreValue {
  type: string
  value: string
}
