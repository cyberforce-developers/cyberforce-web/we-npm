export default interface IActionTrigger {
  type: string
  key: string
}
