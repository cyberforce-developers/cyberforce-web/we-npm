import IKeyAny from '../IKeyAny'

export default interface IActionRequestParams {
  key: string
  onTrigger: any
  onSuccess: any
  onError: any
}
