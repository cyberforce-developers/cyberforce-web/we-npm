import IKeyAny from '../IKeyAny'

export default interface IActionError {
  type: string
  key: string
  error: IKeyAny
}
