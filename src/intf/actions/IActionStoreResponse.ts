export default interface IActionStoreResponse {
  type: string
  key: string
  data: any
}
