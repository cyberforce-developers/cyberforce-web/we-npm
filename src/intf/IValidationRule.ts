/*
  It is a field validation rule definition.
  It can be used in validationSchema.ts of your application.
*/

export default interface IValidationRule {
  min?: number, // minimum length
  max?: number, // maximum length
  mustMatch?: string[], // must match to at least one of the given values
  mustNotMatch?: string[], // must not match to at least one of the values given
  eitherOr?: string, // provide a value or a key to a field (i.e. *phone_2)
  check?: RegExp, // regex for typed characters
  input?: RegExp, // regex for input value
  format?(val: any): any
}
