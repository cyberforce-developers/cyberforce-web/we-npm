import IStaticRoute from './IStaticRoute'

export default interface IStaticRoutes {
  [name: string]: IStaticRoute
}
