import LifeCycles from '../props/LifeCycles'

export default interface IRequestLifeCycle {
  lifecycle: LifeCycles
  status?: number
  data: any
}
