/*
  IValidationCheck is a result of performing Validation.check() method in Validation class.
  It is a final result of validation of all given fields in the model (IModelDataItems)
*/

import IValidationSummary from './modules/IModuleValidationSummary'
import IValidatorCheck from './IValidatorCheck'

export default interface IValidationCheck {
  valid: boolean
  checks: IValidatorCheck[],
  failed: IValidatorCheck[]
}
