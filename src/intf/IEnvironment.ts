import Environments from '../props/Environments'

export default interface IEnvironment {
  name: Environments
  description: string
  compression: boolean
  appDir: string
  port: number
  headers?: string[]
}
