import IEnvironment from './IEnvironment'
import IStaticRoutes from './IStaticRoutes'

export default interface IWebServerConstructor {
  config: IEnvironment
  staticRoutes: IStaticRoutes
}
