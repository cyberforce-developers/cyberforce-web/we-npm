/*
  Translation module enables translation services across the application.
  Public methods for Translation module.
*/

import IKeyAny from '../IKeyAny'

export default interface IModuleLocale {
  hasAnyTranslation(): boolean,
  addTranslation(language: string, translation: IKeyAny): void,
  hasTranslation(language: string): boolean,
  setCurrentLanguage(language: string): void,
  text(key: string, subkey?: string): string | object | any
}
