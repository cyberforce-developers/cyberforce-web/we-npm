/*
  IValidationSummary is a summary of validation performed, that gives developer more control over the validation results
  that can be then translated or displayed on the screen.
*/

export default interface IValidationSummary {
  isValid(): boolean
  getList(): string[]
}
