import IPrismicParsedData from '../IPrismicParsedData'

export default interface IModulePrismic {
    getData(): IPrismicParsedData[]
    getContent(uid: string): IPrismicParsedData[] | []
}
