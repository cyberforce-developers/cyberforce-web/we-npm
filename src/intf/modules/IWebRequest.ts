import { AxiosPromise } from 'axios'
import IKeyAny from '../IKeyAny'
import IApiRoute from '../IApiRoute'
import RequestMethods from '../../props/RequestMethods'

export default interface IWebRequest {
  sendToApiRoute({route, payload, options}: IWebRequestSendToApiRoute): AxiosPromise<any>
  send({url, method, payload, options}: IWebRequestSend): AxiosPromise<any>
  setTesting(sw: boolean): void
}

export interface IWebRequestSendToApiRoute {
  route: IApiRoute
  payload?: IKeyAny
  options?: {
    headers?: IKeyAny
  }
}

export interface IWebRequestSend {
  url: string
  method: RequestMethods
  payload?: IKeyAny
  options?: {
    headers?: IKeyAny
  }
}

export interface IWebRequestMake {
  url: string
  method: RequestMethods
  payload?: IKeyAny
  options?: {
    headers?: IKeyAny
  }
}

export interface IWebRequestArgs {
  baseUrl?: string
  timeout?: number
  headers?: IKeyAny
}
