import IEnvironment from './IEnvironment'

export default interface IWebServerConfig {
  [environment: string]: IEnvironment
}
