/*
  Validator is a module responsible for performing validation on a single field. (We can improve or replace validators keeping interfaces.)
  Result of performing Validator.check() operation is IValidatorCheck response, that can be then used
  to form a Validation Summary and Validation Check with all validated fields together.

  Validation => (n) Validator ... check() => Validation Summary
*/

export default interface IValidatorCheck {
  valid: boolean
  key: string
  validationKey: string | undefined
  label: string
  length: IValidatorCheckLength
  error?: string
  required?: boolean
  mustMatchFields?: string[]
  mustNotMatchFields?: string[]
  optionalButCheckEnforcedIfNotEmpty?: boolean
  eitherOr?: string
}

export interface IValidatorCheckLength {
  expected?: number
  given: number
}
