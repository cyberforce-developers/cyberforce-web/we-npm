/*
  Definition of set of rules for fields validation. Used in validationSchema.ts of a web project.
*/

import IValidationRule from './IValidationRule'

export default interface IValidationSchema {
  [name: string]: IValidationRule
}
