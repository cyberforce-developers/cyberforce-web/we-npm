// Interface that defines a Route within a web application

export default interface IRoute {
  name: string // name of the route, should match the redux directory
  route: string // react-router route map/pattern
  description: string // description what the route component is doing
  layout?: any // layout component (HOC) for the `component`
  component: any // React component linked to the route
  exact?: boolean  // Route exact?
  redirectTo?: string // redirect URL, if given the rest is ignore
  uid?: string // Prismic UID
}
