import IInputModelDataItem from './IInputModelDataItem'

export default interface IInputModelData {
  [key: string]: IInputModelDataItem
}
