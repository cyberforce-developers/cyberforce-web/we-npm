import IWebAppConfig from '../IWebAppConfig'
import IWebAppEnvironment from '../IWebAppEnvironment'
import IWebAppGlobal from '../IWebAppGlobal'
import IRoute from '../IRoute'
import Locale from '../../modules/locale'

export default interface IBootstrapProps {
  route: IRoute
  config: IWebAppConfig
  global: IWebAppGlobal
  current: IWebAppEnvironment
  locale: Locale
}
