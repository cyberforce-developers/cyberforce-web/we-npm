import IBootstrapProps from './IBootstrapProps'
import PrismicContent from '../../modules/prismic/content'

export default interface IMasterProps extends IBootstrapProps {
  prismic: PrismicContent
  children: any
}
