export default interface IStaticRoute {
  uri: string
  directory: string
}
