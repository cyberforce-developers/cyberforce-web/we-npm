// React web application (front-end/client) config

import IWebAppEnvironment from './IWebAppEnvironment'

export default interface IWebAppConfig {
  envs: {
    [environment: string]: IWebAppEnvironment
  }
}
