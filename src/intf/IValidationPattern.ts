/*
  Allows you to define a validation rule for a field, stored in validationSchema of your project
  It is a part of IValidationRule
*/

export default interface IValidationPattern {
  check: RegExp,
  input?: RegExp
}
