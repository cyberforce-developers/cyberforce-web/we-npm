export default interface IKeyString {
  [uri: string]: string
}
