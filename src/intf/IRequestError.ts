export default interface IRequestError {
  status: number
  statusText?: string
  data?: any
}
