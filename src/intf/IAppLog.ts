import LogLevels from '../props/LogLevels'

// Interface for logs in the web application.
// These can be send to Elastic or displayed on screen depending of how you invoke a logger
export default interface IAppLog {
  level: LogLevels
  slug: string,
  message?: string
}
