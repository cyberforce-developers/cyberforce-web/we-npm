import RequestMethods from '../props/RequestMethods'

export default interface IApiRoute {
  uri: string
  method: RequestMethods
  auth: boolean
  controller: any
}
