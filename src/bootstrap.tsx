import * as React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { createStore, combineReducers, applyMiddleware, compose, Store } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'

import { Utils } from './modules/utils'

import IWebAppBootstrap from './intf/IWebAppBootstrap'
import IRoute from './intf/IRoute'
import IWebAppGlobal from './intf/IWebAppGlobal'
import IWebAppConfig from './intf/IWebAppConfig'
import IKeyAny from './intf/IKeyAny'
import IWebAppEnvironment from './intf/IWebAppEnvironment'
import IBootstrapProps from './intf/react/IBootstrapProps'
import Locale from './modules/locale'

const renderRoute = (route: IRoute, global: IWebAppGlobal, config: IWebAppConfig, current: IWebAppEnvironment, locale: Locale, BootstrapComponent: any): JSX.Element => {
  if (route.redirectTo) {
    return (
      <Redirect
        key={Utils.ID({ pattern: '4.1234' })}
        from={route.route}
        to={route.redirectTo}
      />
    )
  } else {
    let componentProps: IBootstrapProps = {
      route,
      config: {...config},
      global: {...global},
      current: {...current},
      locale
    }

    let FinalComponent: any
    const Component: any = route.component

    if (route.layout) {
      const Layout: any = route.layout
      FinalComponent = (
        <BootstrapComponent {...componentProps}>
          <Layout {...componentProps}>
            <Component {...componentProps} />
          </Layout>
        </BootstrapComponent>
      )
    } else {
      FinalComponent = (
        <BootstrapComponent {...componentProps}>
          <Component {...componentProps} />
        </BootstrapComponent>
      )
    }

    const props: IKeyAny = {
      path: route.route,
      exact: route.exact || false,
      component: () => FinalComponent
    }

    return (
      <Route
        key={Utils.ID({ pattern: '16.1234567890' })}
        {...props}
      />
    )
  }
}

const reducers = (routes: IRoute[]): IKeyAny => {
  const reducersList: IKeyAny = {}
  const names = routes.map((route: IRoute) => route.name)
  names.forEach((name: string) => {
    let reducer: any = null
    const rName = name.toLowerCase()
    try {
      reducer = require(`../../../src/redux/pages/${rName}/state`).default
    } catch (err) {
      console.error(`Reducer for ${name} does not exist`)
    }
    if (reducer) {
      reducersList[`r_${rName}`] = reducer
    }
  })
  return reducersList
}

const getStore = (routes: IRoute[], enhancers?: IKeyAny): Store => {
  const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose
  let reducersList: IKeyAny = reducers(routes)
  if (enhancers) {
    reducersList = {
      ...reducersList,
      ...enhancers
    }
  }
  return createStore(combineReducers(reducersList), composeEnhancers(applyMiddleware(thunk)))
}

export const boot = ({routes, global, config, current, locale, enhancers, bootstrapComponent}: IWebAppBootstrap) => {

  let routesComponents: JSX.Element[] = []
  routesComponents = routes.map((route: IRoute) => renderRoute(route, global, config, current, locale, bootstrapComponent))

  return (
    <Provider store={getStore(routes, enhancers)}>
    <BrowserRouter>
      <Switch>
        {...routesComponents}
      </Switch>
    </BrowserRouter>
    </Provider>
  )
}
