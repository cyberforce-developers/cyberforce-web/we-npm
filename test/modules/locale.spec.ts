import Locale from '../../src/modules/locale'
import Languages from '../../src/props/Languages'
import { expect } from 'chai'

const tUs = require('../support/translations/en-US')
const tGb = require('../support/translations/en-GB')

describe('Locale', () => {
  const gb = Languages.gb
  const us = Languages.us
  let locale: Locale

  before(() => {
    locale = new Locale()
    locale.addTranslation(gb, tGb)
    locale.setCurrentLanguage(gb)
  })

  it('should return a valid translation from locale', () => {
    const genre = locale.text('genre')
    expect(genre).to.have.property('f')
    expect(genre.f).to.equal(tGb.genre.f)
  })

  it('returns set of properties', () => {
    const genre: object = locale.text('genre')
    expect(genre).to.have.property('f')
    expect(genre).to.have.property('m')
  })

  it('returns translated language property', () => {
    const f = locale.text('genre', 'f')
    expect(f).to.equal(tGb.genre.f)
  })

  it('merges correctly another translation added to the existing one', () => {
    const otherGb = {
      realm: {
        north: 'North',
        south: 'South'
      }
    }
    locale.addTranslation(gb, otherGb)
    const prev = locale.text('genre', 'm')
    const txt = locale.text('realm', 'north')
    expect(prev).to.equal(tGb.genre.m)
    expect(txt).to.equal(otherGb.realm.north)
  })

  it('returns text from other translation if switched', () => {
    locale.addTranslation(us, tUs)
    locale.setCurrentLanguage(us)
    const txt = locale.text('genre', 'm')
    expect(txt).to.equal(tUs.genre.m)
  })

  it('returns error when language is unknown', () => {
    const unknownLocale = new Locale()
    const hasTranslation = unknownLocale.hasTranslation(us)
    expect(hasTranslation).to.equal(false)
  })
})
