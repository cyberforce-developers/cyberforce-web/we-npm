import { Utils } from '../../src/modules/utils'
import { expect } from 'chai'
import Languages from '../../src/props/Languages'

describe('Utils', () => {

  describe('ID', () => {

    it('generated a 16 character length string hexadecimal', () => {
      const str: string = Utils.ID({ pattern: '16.abcdef0123456789' })
      expect(str).to.have.length(16)
      expect(str).to.match(/[a-f0-9]/)
    })

  })

  describe('hash', () => {
    it('correctly hashes a string', () => {
      const str: string = Utils.hash('thisIsMyPassword')
      expect(str).to.eql('80d2f0dd3f1caa2e62bab686d6d1d140')
    })
  })

  describe('parsePrismicLanguage', () => {

    it('returns correct language abbreviation from Prismic abbrev', () => {
      expect(Utils.parsePrismicLanguage('en-gb')).to.equal(Languages.gb)
    })

  })

})
