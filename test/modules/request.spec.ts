import { expect } from 'chai'
import WebRequest from '../../src/modules/request/web'

import IApiRoute from '../../src/intf/IApiRoute'
import RequestMethods from '../../src/props/RequestMethods'
import testController from '../support/api/endpoint'

describe('Web request', () => {

  describe('with api route', () => {

    const route: IApiRoute = {
      uri: '/test',
      method: RequestMethods.GET,
      controller: testController,
      auth: false
    }

    it('sends correctly a `GET` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const res = await req.sendToApiRoute({route})
      expect(res.status).to.eql(200)
    })

    it('sends correctly a parametrised `GET` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const payload = {
        name: 'John',
        surname: 'Doe'
      }
      const res = await req.sendToApiRoute({ route, payload })
      expect(res.status).to.eql(200)
      expect(res.data.options).to.have.property('params')
      expect(res.data.options.params).to.eql(payload)
    })

    it('checks default json/utf-8 header for `GET` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const res = await req.sendToApiRoute({ route })
      expect(res.status).to.eql(200)
      expect(res.data.options).to.have.property('headers')
      expect(res.data.options.headers).to.contain({'Content-Type': 'application/json; charset=utf-8'})
    })

    it('sends correctly a `GET` request with custom headers', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const headers = {
        'X-Header': 'test-header'
      }
      const res = await req.sendToApiRoute({ route, options: { headers } })
      expect(res.status).to.eql(200)
      expect(res.data.options).to.have.property('headers')
      expect(res.data.options.headers).to.contain(headers)
    })

    it('sends correctly a `POST` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const otherRoute: IApiRoute = {
        ...route,
        method: RequestMethods.POST,
      }
      const res = await req.sendToApiRoute({ route: otherRoute })
      expect(res.status).to.eql(200)
    })

    it('checks default json/utf-8 header for `POST` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const otherRoute: IApiRoute = {
        ...route,
        method: RequestMethods.POST,
      }
      const res = await req.sendToApiRoute({ route: otherRoute })
      expect(res.status).to.eql(200)
      expect(res.data.options).to.have.property('headers')
      expect(res.data.options.headers).to.contain({'Content-Type': 'application/json; charset=utf-8'})
    })

    it('checks passing payload for `POST` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const otherRoute: IApiRoute = {
        ...route,
        method: RequestMethods.POST,
      }
      const payload = {
        name: 'John',
        surname: 'Doe'
      }
      const res = await req.sendToApiRoute({ route: otherRoute, payload })
      expect(res.status).to.eql(200)
      expect(res.data).to.have.property('payload')
      expect(res.data.payload).to.contain(payload)
    })

    it('checks custom headers for `POST` request', async () => {
      const req = new WebRequest()
      req.setTesting(true)
      const otherRoute: IApiRoute = {
        ...route,
        method: RequestMethods.POST,
      }
      const headers = {
        'X-Header': 'test-header'
      }
      const res = await req.sendToApiRoute({
        route: otherRoute,
        options: {
          headers
        }
      })
      expect(res.status).to.eql(200)
      expect(res.data.options).to.have.property('headers')
      expect(res.data.options.headers).to.contain({'Content-Type': 'application/json; charset=utf-8'})
      expect(res.data.options.headers).to.contain(headers)
    })
  })

  describe('with normal URL', () => {
    // TODO
  })

})
