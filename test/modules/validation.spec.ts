import schema from '../support/exampleValidationSchema'
import { expect } from 'chai'
import { InputModelFactory } from '../support/factories/model'
import IInputModelDataItem from '../../src/intf/IInputModelDataItem'
import Locale from '../../src/modules/locale'
import Languages from '../../src/props/Languages'
const enUS = require('../support/translations/en-US')

import Validation from '../../src/modules/validation/validation'
import ValidationErrors from '../../src/props/ValidationErrors'
import ValidationSummary from '../../src/modules/validation/summary'

const matches: string[] = ['male', 'female'] // user_gender

describe('Validation check', () => {

  it('the correct value passes validation', () => {
    const model = InputModelFactory.defaultRegistration()
    const name: IInputModelDataItem = model.getInputDataItem('name')

    const v = new Validation({ name }, schema, [])
    const result = v.check()
    expect(result).to.exist
    expect(result.valid).to.be.true
    expect(result.checks.length).to.equal(1)
    expect(result.checks[0].key).to.equal('name')
    expect(result.checks[0].length.given).to.equal(name._.length)
    expect(result.checks[0].length.expected).to.not.exist
  })

  it('model factory correctly merges the data', () => {
    const input = {
      surname: {
        _: '29493499439'
      }
    }
    const model = InputModelFactory.defaultRegistration(input)
    const surname: IInputModelDataItem = model.getInputDataItem('surname')
    expect(surname._).equal(input.surname._)
    expect(surname.v).equal('user_surname') // from the factory
  })

  it('the incorrect field value not passes validation', () => {
    const model = InputModelFactory.defaultRegistration({
      surname: {
        _: '29493499439'
      }
    })
    const data = model.getInputData()
    const v = new Validation(data, schema)
    const result = v.check()
    expect(result).to.exist
    expect(result.valid).to.be.false
    expect(result.checks.length).to.be.greaterThan(1)
    expect(result.checks[1].key).to.equal('surname')
    expect(result.checks[1].error).to.equal(ValidationErrors.regex)
  })

  it('must keep a minimum length', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'g'
      }
    })
    const record = model.getInputDataItem('name')
    const v = new Validation(model.getInputData(), schema)
    const result = v.check()
    const rule = (record.v)
      ? schema[record.v]
      : undefined

    expect(rule).exist
    expect(result.valid).to.be.false
    expect(result.checks[0].length.given).to.equal(1)
    expect(result.checks[0].error).to.equal(ValidationErrors.tooShort)
    if (rule) {
      expect(result.checks[0].length.expected).to.equal(rule.min)
    }
  })

  it('must keep a maximum length of string', () => {
    const sampleName = 'dannishhussainintouchnetworks'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: sampleName
      }
    })
    const record = model.getInputDataItem('name')
    const validation = new Validation(model.getInputData(), schema)
    const result = validation.check()
    const rule = (record.v) ? schema[record.v] : undefined

    expect(rule).exist
    expect(result.valid).to.be.false
    expect(result.checks[0].length.given).to.equal(sampleName.length)
    expect(result.checks[0].error).to.equal(ValidationErrors.tooLong)
    if (rule) {
      expect(result.checks[0].length.expected).to.equal(rule.max)
    }
  })

  it('value must match to at least one given in array ', () => {
    const model = InputModelFactory.defaultRegistration({
      gender: {
        _: matches[0],
        v: 'user_gender'
      }
    })

    const newSchema = {
      ...schema,
      user_gender: {
        mustMatch: matches
      }
    }

    const validation = new Validation(model.getInputData(), newSchema)
    const result = validation.check()
    expect(result.valid).to.be.true
  })

  it('does not pass a validation when a value does not match to at least one element in array while it should', () => {
    const model = InputModelFactory.defaultRegistration({
      gender: {
        _: 'dummy'
      }
    })

    const newSchema = {
      ...schema,
      user_gender: {
        mustMatch: matches
      }
    }

    const validation = new Validation(model.getInputData(), newSchema)
    const result = validation.check()
    expect(result.valid).to.be.false
    expect(result.failed[0].key).to.equal('gender')
    expect(result.failed[0].error).to.equal(ValidationErrors.mustMatch)
  })

  it('must match to a dynamic value of another field within a model', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: val
      },
      nickname: {
        _: val
      }
    })

    const rules = {
      ...schema,
      user_nickname: {
        mustMatch: ['*name']
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.true
  })

  it('does not pass validation when one field value does not match to another field value', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'someexample'
      },
      nickname: {
        _: val
      }
    })

    const rules = {
      ...schema,
      user_nickname: {
        mustMatch: ['*name']
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.false
    expect(result.failed[0].error).to.equal(ValidationErrors.mustMatch)
  })

  it('must not match to a dynamic value of another field within a model to pass the validation', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'somevalue'
      },
      nickname: {
        _: val
      }
    })

    const rules = {
      ...schema,
      user_nickname: {
        mustNotMatch: ['*name']
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.true
  })

  it('does not pass validation when one field value matches to another field value', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: val
      },
      nickname: {
        _: val
      }
    })
    const rules = {
      ...schema,
      user_nickname: {
        mustNotMatch: ['*name']
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.false
    expect(result.failed[0].error).to.equal(ValidationErrors.mustNotMatch)
  })

  it('passes when the dynamic value of two is defined, given `eitherOr` rule is set', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: undefined
      },
      nickname: {
        _: val
      }
    })

    const rules = {
      ...schema,
      user_name: {
        eitherOr: '*nickname'
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.true
  })

  it('passes when the value of two is defined assuming it is a fixed value and given `eitherOr` rule is set', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: undefined
      },
      nickname: {
        _: val
      }
    })

    const rules = {
      ...schema,
      user_name: {
        eitherOr: ''
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.true
  })

  it('does not pass the validation when the dynamic value of either two fields is defined, given `eitherOr` rule is set', () => {
    const val = 'outcast'
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: undefined
      },
      nickname: {
        _: undefined
      }
    })

    const rules = {
      ...schema,
      user_name: {
        eitherOr: '*nickname'
      }
    }

    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.false
    expect(result.failed[0].error).to.equal(ValidationErrors.eitherOr)
    expect(result.failed[0].eitherOr).to.equal('*nickname')
  })

  it('should pass depending on `isValidWhenNoRule` setting if the rule is empty', () => {
    const model = InputModelFactory.defaultRegistration()
    const rules = { ...schema }
    delete rules.user_name
    const validation = new Validation(model.getInputData(), rules)
    const result = validation.check()
    expect(result.valid).to.be.true
    // switch manually isValidWhenNoRule to false in validator.ts to test, or increase visibility for a class decorator
    // setIsValidWhenNoRule by optional parameters (in Validation) class
  })

})

describe('Validation summary', () => {

  const locale = new Locale()
  locale.addTranslation(Languages.us, enUS)
  locale.setCurrentLanguage(Languages.us)

  it('returns a validation summary when validation did not pass', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'incorrectN1me'
      },
      surname: {
        _: '129348858'
      },
      code: {
        _: 'abcdefghi'
      }
    })

    const validation = new Validation(model.getInputData(), schema)
    const result = validation.check()
    const summary = new ValidationSummary(result, locale)
    const list = summary.getList()
    expect(list).to.have.length(3)
    expect(summary.isValid()).to.be.false
  })

  it('returns empty validation summary when validation passed', () => {
    const model = InputModelFactory.defaultRegistration()
    const validation = new Validation(model.getInputData(), schema)
    const result = validation.check()
    const summary = new ValidationSummary(result, locale)
    expect(summary.getList()).to.have.length(0)
    expect(summary.isValid()).to.be.true
  })

  it('returns custom validation message when regex is errorous', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'incorrectN1me'
      }
    })
    const validation = new Validation(model.getInputData(), schema)
    const result = validation.check()
    const summary = new ValidationSummary(result, locale)
    const list = summary.getList()
    const message = locale.text('inputs', 'name')
    expect(result.failed[0].error).to.equal(ValidationErrors.regex)
    expect(list).to.have.length(1)
    expect(summary.isValid()).to.be.false
    expect(message.validation).to.exist
    expect(list[0]).to.contain(message.validation)
  })

})
