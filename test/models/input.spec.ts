import { expect } from 'chai'
import { InputModelFactory } from '../support/factories/model'
import * as patterns from '../../src/props/ValidationPatterns'
import schema from '../support/exampleValidationSchema'
import LifeCycles from '../../src/props/LifeCycles'

describe('Input model', () => {

  it('returns correct input data', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory'
      }
    })
    model.setSchema(schema)
    const data = model.getInputData()
    const keys = Object.keys(data)
    expect(data).to.have.property('name')
    expect(keys.length).to.be.greaterThan(0)
    expect(keys).to.include('name')
    expect(data['name']._).to.equal('gregory')
  })

  it('returns a correct input data item', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory',
        l: 'stuff',
        v: 'somekey'
      }
    })
    model.setSchema(schema)

    const name = model.getInputDataItem('name')
    expect(name).to.exist
    expect(name._).to.equal('gregory')
    expect(name.l).to.equal('stuff')
    expect(name.v).to.equal('somekey')
  })

  it('returns x if item is not there', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    const field = model.getInputDataItem('nonexistingfield')
    expect(field).to.exist
    expect(field._).to.equal('')
    expect(field.v).to.not.exist
    expect(field.l).to.not.exist
  })

  it('returns a correct field value', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory'
      }
    })
    model.setSchema(schema)
    const name = model.getValue('name')
    expect(name).to.equal('gregory')
  })

  it('returns an empty value for an incorrect field key', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory'
      }
    })
    model.setSchema(schema)
    const name = model.getValue('xxx')
    expect(name).to.be.empty
  })

  it('sets correctly a new value for a field', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory'
      }
    })
    model.setSchema(schema)
    model.setValue('name', 'tom')
    const name = model.getValue('name')
    expect(name).to.equal('tom')
  })

  it('forbids to set a new value if a field does have `exact` property specified and does not match `input` regex', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory',
        exact: true
      }
    })
    model.setSchema(schema)
    model.setValue('name', '123845')
    const name = model.getValue('name')
    expect(name).to.equal('gregory')
  })

  it('allows to set a new value if a field does have `exact` property specified and matches `input` regex', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory',
        exact: true
      }
    })
    model.setSchema(schema)
    model.setValue('name', 'tom')
    const name = model.getValue('name')
    expect(name).to.equal('tom')
  })

  it('sets a custom schema', () => {
    const model = InputModelFactory.defaultRegistration({
      name: {
        _: 'gregory',
        exact: true
      }
    })
    model.setSchema({
      ...schema,
      user_name: {
        ...patterns.onlyNumbers
      }
    })
    model.setValue('name', 'tom')
    let name = model.getValue('name')
    expect(name).to.equal('gregory')
    model.setSchema({
      ...schema,
      user_name: {
        ...patterns.smallLetters
      }
    })
    model.setValue('name', 'tom')
    name = model.getValue('name')
    expect(name).to.equal('tom')
  })

  it('correctly set lifecycle', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    expect(model.isRequestTriggered()).to.be.false
    model.setLifeCycle(LifeCycles.triggered)
    expect(model.isRequestTriggered()).to.be.true
  })

  it('correctly check completed request', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    expect(model.isRequestCompleted()).to.be.false
    model.setLifeCycle(LifeCycles.completed)
    expect(model.isRequestCompleted()).to.be.true
  })

  it('correctly indicated idle status when initialised', () => {
    const model = InputModelFactory.defaultRegistration()
    expect(model.isIdle()).to.be.true
  })

  it('correctly sets the processed status of a lifecycle', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    model.validate()
    expect(model.isValidationPerformed()).to.be.true
    model.setLifeCycleProcessed()
    expect(model.isProcessed()).to.be.true
    expect(model.isValidationPerformed()).to.be.false
  })

  it('correctly check if validation has been performed', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    expect(model.isValidationPerformed()).to.be.false
    expect(model.getValidation().valid).to.be.false
    model.validate()
    expect(model.isValidationPerformed()).to.be.true
  })

  it('correctly sets API response', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    const response = {
      status: 'success',
      statusCode: 200,
      data: {
        name: 'John',
        surname: 'Doe'
      }
    }
    model.setResponse(response)
    const resp = model.getResponse()
    expect(resp).to.eql(response)
  })

  it('returns information about validity of a particular field when incorrect', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    model.setValue('name', '12345', true)
    model.validate()
    expect(model.isFieldValid('name')).to.be.false
  })

  it('returns information about validity of a particular field when correct', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    model.setValue('name', 'adam')
    model.validate()
    expect(model.isFieldValid('name')).to.be.true
  })

  it('correctly validates input for all fields', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema({
      ...schema,
      user_name: {
        ...patterns.onlyNumbers
      }
    })
    model.validate()
    const result = model.isValid()
    expect(result).to.be.false
  })

  it('correctly validates input for a given field', () => {
    const model = InputModelFactory.defaultRegistration()
    model.setSchema(schema)
    model.setValue('code', 'abcdefg')
    model.validate(['name'])
    const result = model.isValid()
    expect(result).to.be.true
  })

})
