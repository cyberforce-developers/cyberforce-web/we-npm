import * as React from 'react'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import 'jsdom-global/register'
import { expect } from 'chai'
import { spy } from 'sinon'
import InputText from '../../src/components/InputText/InputText'

Enzyme.configure({ adapter: new Adapter() })
spy(InputText.prototype, 'render')

describe('<InputText />', () => {

  const el = Enzyme.mount(<InputText />)

  // Kept as an example for spies and sinon
  it('calls render once', () => {
    expect(InputText.prototype.render).to.have.property('callCount', 1)
  })

  it('returns input', () => {
    expect(el.find('input').exists()).to.equal(true)
  })

  it('has set an attribute `name`', () => {
    const name = el.find('input').prop('name')
    expect(name).to.exist
    expect(name).to.have.length.greaterThan(0)
  })

  it('has set an attribute `type`', () => {
    const type = el.find('input').prop('type')
    expect(type).to.exist
    expect(type).to.eql('input')
  })

  it('has an attribute `value`', () => {
    const value = el.find('input').prop('value')
    expect(value).to.exist
  })

  it('has a defined attribute `onChange` function', () => {
    const input = Enzyme.mount(<InputText onChange={() => console.log('x')} />).find('input')
    const onChange = input.prop('onChange')
    expect(onChange).to.exist
  })

})
