import * as deepmerge from 'deepmerge'
import * as patterns from '../../src/props/ValidationPatterns'
import IValidationSchema from '../../src/intf/IValidationSchema'

const RealDataSchema: IValidationSchema = {
  user_name: {
    min: 3,
    max: 16,
    ...patterns.smallLetters
  },
  user_surname: {
    min: 2,
    max: 48,
    ...patterns.smallLetters
  }
}

const VirtualDataSchema: IValidationSchema = {
  digits: {
    min: 2,
    ...patterns.onlyNumbers
  }
}

export default deepmerge(RealDataSchema, VirtualDataSchema)
