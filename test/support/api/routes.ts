import IRoute from '../../../src/intf/IApiRoute'
import RequestMethods from '../../../src/props/RequestMethods'
import testController from './endpoint'

export const jobs: IRoute[] = [
  {
    uri: '/jobs',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/jobs/add',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]

export const location: IRoute[] = [
  {
    uri: '/locations',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/locations/add',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]

export const profile: IRoute[] = [
  {
    uri: '/profile/view',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/profile/list',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]
