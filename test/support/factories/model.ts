import InputModel from '../../../src/models/input'
import IInputModelData from '../../../src/intf/IInputModelData'
import * as deepmerge from 'deepmerge'

export namespace InputModelFactory {

  export function defaultRegistration(data?: IInputModelData): InputModel {
    const pre: IInputModelData = {
      name: {
        _: 'john',
        v: 'user_name'
      },
      surname: {
        _: 'doe',
        v: 'user_surname'
      },
      gender: {
        _: 'female',
        v: 'user_gender'
      },
      nickname: {
        _: '',
        v: 'user_nickname'
      },
      code: {
        _: '12948458',
        v: 'digits',
        exact: true
      }
    }
    const input = (data) ? deepmerge(pre, data) : pre

    return new InputModel({
      name: 'registration',
      data: input
    })
  }

}
