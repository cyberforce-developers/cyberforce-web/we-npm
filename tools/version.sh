#!/bin/bash

if [ "$1" != "" ]; then
  echo " "
  echo "----------------------------------------------------------------"
  echo "- Generating package.json for @cyberforce/web-essentials package -"
  echo "- Version: $1                                               -"
  echo "----------------------------------------------------------------"
  echo " "
  content="
{
  \"name\": \"@cyberforce/web-essentials\",
  \"version\": \"$1\",
  \"author\": \"Grzegorz Wozniak\",
  \"copyright\": \"All rights reserved (C) 2019 Cyberforce\",
  \"description\": \"ITN web framework containing shared and reusable modules and parts for internal web applications \",
  \"homepage\": \"https://gitlab.com/cyberforce-developers/cyberforce-web/we-npm.git#README\",
  \"keywords\": [
    \"intouchnetworks\",
    \"framework\",
    \"itn-web\"
  ],
  \"license\": \"ISC\",
  \"repository\": {
    \"type\": \"git\",
    \"url\": \"git@gitlab.com:cyberforce-developers/cyberforce-web/we-npm.git\"
  },
  \"dependencies\": {
    \"crypto-js\": \"^3.1.9-1\"
  }
}"
  echo "$content" > ./dist/package.json
  echo "Success."
else
    echo "Version number must be defined"
fi